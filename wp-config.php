<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'iked_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[eECm_#R<V_*+wdrsYYFwF /y:?w+S `ne.OI(`m?[pKXPX^[[wKG9j0#np+TxzQ');
define('SECURE_AUTH_KEY',  '^65fU@4W7f4~se4I]1g.K,,=J{sH|^5B9>!gXVVnuyLG I=MD</qRPef8Hp`5;!o');
define('LOGGED_IN_KEY',    '#52s|d8nb|I}^T|/pXc=O-V;}$+V[:F@g%!nJ^MCwz8!wS p_*I[TfI/=y~T/*55');
define('NONCE_KEY',        'aQ`-m|Lc{I|Q0ru8}`#B,ge5%~F}Q*, ^0sG(BZ)c`ex{;7OyMBfK:mm54s:K#(6');
define('AUTH_SALT',        '`E,+w$pfsh:or)[vMF@[~>]H0Q2qP[pdw^u&f ?cBF-zU`HsJDR2h-F sK34>N)I');
define('SECURE_AUTH_SALT', '4%ZR7*`{nstyiz`%vBe+BxcrM|!rv^;.Cz}Ioy$y_V(k,%lN1~pzXQ8t,7N.t!o1');
define('LOGGED_IN_SALT',   'GdNnLk{u?s]@=!97K)j5~<]O~h?3H?wit#O#U*u,*SQvdX0x;@<~/z r.0nGKWCt');
define('NONCE_SALT',       '%Qp@bf|5A)5}xvzm{,5=k||VAWy$3{_p%$zR$Z)RUy%aL[vfi<:W$4u52Rs.G;M3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
